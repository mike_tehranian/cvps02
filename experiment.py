"""
CS6476: Problem Set 2 Experiment file

This script contains a series of function calls that run your ps2
implementation and output images so you can verify your results.
"""


import cv2

import ps2

# TODO
# Search by each color channel, if multiple matches search and see if each one is lite.
# If then can't find center well, define a ROI and search circles again with new parameters.
# For part 1, if the circle appears twice in the mask, I assume you can assume it has to
#    be the sun and the mask is yellow. Could simplify the coding

# use green mask to find the green traffic light and the other lights are right next to it
# the green circle is unique in all of the photo so it is a unique feature to find

#
# Need to determine the size of the bins for Hough Accumulator Array
# Use domain knowledge about the number of expected peaks to tune transform
# Look at using Gradient extension for faster performance
# Hierarchael binning: use large then small sizes
#

# OH - TA had to do a lot of parameter tuning of Canny for the noisy images
#
def draw_tl_center(image_in, center, state):
    """Marks the center of a traffic light image and adds coordinates
    with the state of the current image

    Use OpenCV drawing functions to place a marker that represents the
    traffic light center. Additionally, place text using OpenCV tools
    that show the numerical and string values of the traffic light
    center and state. Use the following format:

        ((x-coordinate, y-coordinate), 'color')

    See OpenCV's drawing functions:
    http://docs.opencv.org/2.4/modules/core/doc/drawing_functions.html

    Make sure the font size is large enough so that the text in the
    output image is legible.
    Args:
        image_in (numpy.array): input image.
        center (tuple): center numeric values.
        state (str): traffic light state values can be: 'red',
                     'yellow', 'green'.

    Returns:
        numpy.array: output image showing a marker representing the
        traffic light center and text that presents the numerical
        coordinates with the traffic light state.
    """
    image_out = image_in
    cv2.circle(image_out, center, 3, (255,0,0), -1)
    x,y = center
    text = '(({}, {}), \'{}\')'.format(x,y,state)
    x=x+15
    if image_out.shape[1] - x < 60:
        x = x - 70
    font = cv2.FONT_HERSHEY_SIMPLEX
    cv2.putText(image_out, text, (x, y+40), font, 0.5, (255,0,0), 1)

    cv2.imshow('draw', image_out)
    cv2.waitKey()
    return image_out


def mark_traffic_signs(image_in, signs_dict):
    """Marks the center of a traffic sign and adds its coordinates.

    This function uses a dictionary that follows the following
    structure:
    {'sign_name_1': (x, y), 'sign_name_2': (x, y), etc.}

    Where 'sign_name' can be: 'stop', 'no_entry', 'yield',
    'construction', and 'warning'.

    Use cv2.putText to place the coordinate values in the output
    image.

    Args:
        image_in (numpy.array): the image to draw on.
        signs_dict (dict): dictionary containing the coordinates of
        each sign found in a scene.

    Returns:
        numpy.array: output image showing markers on each traffic
        sign.
    """
    image_out = image_in
    for name, center in signs_dict.iteritems():
        cv2.circle(image_out, center, 3, (255,0,0), -1)
        x,y = center
        coordinates = '({}, {})'.format(x,y)
        # import ipdb; ipdb.set_trace()
        x=x+15
        if image_out.shape[1] - x < 60:
            x = x - 70
        font = cv2.FONT_HERSHEY_SIMPLEX
        cv2.putText(image_out, coordinates, (x, y+20), font, 0.5, (255,0,0), 1)
        cv2.putText(image_out, name, (x, y+40), font, 0.5, (255,0,0), 1)
        #text = '(({}, {}), {})'.format(x,y,name)
        #cv2.putText(image_out, text, (x, y+20), font, 0.5, (255,0,0), 1)

        cv2.imshow('draw', image_out)
        cv2.waitKey()

    return image_out


def part_1():

    input_images = ['simple_tl', 'scene_tl_1', 'scene_tl_2', 'scene_tl_3']
    output_labels = ['ps2-1-a-1', 'ps2-1-a-2', 'ps2-1-a-3', 'ps2-1-a-4']

    #input_images = ['simple_tl']
    #output_labels = ['ps2-1-a-1']
    #input_images = ['scene_tl_1']
    #output_labels = ['ps2-1-a-2']
    #input_images = ['scene_tl_2']
    #output_labels = ['ps2-1-a-3']
    #input_images = ['scene_tl_3']
    #output_labels = ['ps2-1-a-4']

    # PS2 Unit Tests:
    #input_images = ['test_images/simple_tl_test']
    #output_labels = ['ps2-1-a-5']

    # Define a radii range, you may define a smaller range based on your
    # observations.
    radii_range = range(10, 30, 1)

    for img_in, label in zip(input_images, output_labels):
        tl = cv2.imread("input_images/{}.png".format(img_in))

        coords, state = ps2.traffic_light_detection(tl, radii_range)

        img_out = draw_tl_center(tl, coords, state)
        cv2.imwrite("output/{}.png".format(label), img_out)


def part_2():

    input_images = ['scene_dne_1', 'scene_stp_1', 'scene_constr_1',
                    'scene_wrng_1', 'scene_yld_1']

    output_labels = ['ps2-2-a-1', 'ps2-2-a-2', 'ps2-2-a-3', 'ps2-2-a-4',
                     'ps2-2-a-5']

    sign_fns = [ps2.do_not_enter_sign_detection, ps2.stop_sign_detection,
                ps2.construction_sign_detection, ps2.warning_sign_detection,
                ps2.yield_sign_detection]

    sign_labels = ['no_entry', 'stop', 'construction', 'warning', 'yield']

    #input_images = ['scene_dne_1']
    #output_labels = ['ps2-2-a-1']
    #sign_fns = [ps2.do_not_enter_sign_detection]
    #sign_labels = ['no_entry']

    #input_images = ['scene_stp_1']
    #output_labels = ['ps2-2-a-2']
    #sign_fns = [ps2.stop_sign_detection]
    #sign_labels = ['stop']

    #input_images = ['scene_yld_1']
    #output_labels = ['ps2-2-a-5']
    #sign_fns = [ps2.yield_sign_detection]
    #sign_labels = ['yield']

    # input_images = ['scene_constr_1']
    # output_labels = ['ps2-2-a-3']
    # sign_fns = [ps2.construction_sign_detection]
    # sign_labels = ['construction']

    #input_images = ['scene_wrng_1']
    #output_labels = ['ps2-2-a-4']
    #sign_fns = [ps2.warning_sign_detection]
    #sign_labels = ['warning']

    for img_in, label, fn, name in zip(input_images, output_labels, sign_fns,
                                       sign_labels):

        sign_img = cv2.imread("input_images/{}.png".format(img_in))
        coords = fn(sign_img)

        temp_dict = {name: coords}
        img_out = mark_traffic_signs(sign_img, temp_dict)
        cv2.imwrite("output/{}.png".format(label), img_out)


def part_3():

    input_images = ['scene_some_signs', 'scene_all_signs']
    output_labels = ['ps2-3-a-1', 'ps2-3-a-2']

    #input_images = ['scene_some_signs']
    #output_labels = ['ps2-3-a-1']

    #input_images = ['scene_all_signs']
    #output_labels = ['ps2-3-a-2']

    for img_in, label in zip(input_images, output_labels):

        scene = cv2.imread("input_images/{}.png".format(img_in))
        coords = ps2.traffic_sign_detection(scene)

        img_out = mark_traffic_signs(scene, coords)
        cv2.imwrite("output/{}.png".format(label), img_out)


def part_4():
    input_images = ['scene_some_signs_noisy', 'scene_all_signs_noisy']
    output_labels = ['ps2-4-a-1', 'ps2-4-a-2']

    #input_images = ['scene_some_signs_noisy']
    #output_labels = ['ps2-4-a-1']

    #input_images = ['scene_all_signs_noisy']
    #output_labels = ['ps2-4-a-2']

    for img_in, label in zip(input_images, output_labels):
        scene = cv2.imread("input_images/{}.png".format(img_in))
        coords = ps2.traffic_sign_detection_noisy(scene)

        img_out = mark_traffic_signs(scene, coords)
        cv2.imwrite("output/{}.png".format(label), img_out)


def part_5a():
    input_images = ['img-5-a-1', 'img-5-a-2', 'img-5-a-3']
    output_labels = ['ps2-5-a-1', 'ps2-5-a-2', 'ps2-5-a-3']

    for img_in, label in zip(input_images, output_labels):
        scene = cv2.imread("input_images/{}.png".format(img_in))
        coords = ps2.traffic_sign_detection_challenge(scene)

        img_out = mark_traffic_signs(scene, coords)
        cv2.imwrite("output/{}.png".format(label), img_out)


def part_5b():
    input_images = ['img-5-b-1', 'img-5-b-2', 'img-5-b-3']
    output_labels = ['ps2-5-b-1', 'ps2-5-b-2', 'ps2-5-b-3']

    for img_in, label in zip(input_images, output_labels):
        scene = cv2.imread("input_images/{}.png".format(img_in))
        coords = ps2.traffic_sign_detection_challenge(scene)

        img_out = mark_traffic_signs(scene, coords)
        cv2.imwrite("output/{}.png".format(label), img_out)

if __name__ == '__main__':
    part_1()
    part_2()
    part_3()
    part_4()
    part_5a()
    part_5b()
