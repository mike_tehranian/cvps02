"""
CS6476 Problem Set 2 imports. Only Numpy and cv2 are allowed.
"""
import cv2

import numpy as np


def show_lines_for_hough(img_in, hough):
    line_img_in = np.copy(img_in)
    rho, theta = hough
    a = np.cos(theta)
    b = np.sin(theta)
    x0 = a*rho
    y0 = b*rho
    x1 = int(x0 + 1000*(-b))
    y1 = int(y0 + 1000*(a))
    x2 = int(x0 - 1000*(-b))
    y2 = int(y0 - 1000*(a))
    # x1 = int(x0 + (-b))
    # y1 = int(y0 + (a))
    # x2 = int(x0 - (-b))
    # y2 = int(y0 - (a))

    cv2.line(line_img_in,(x1,y1),(x2,y2),(0,255,255),2)

    #cv2.imshow('show_lines_for_hough: {}'.format(hough), line_img_in)
    #cv2.waitKey()


def hough_lines_with_angle(img_in, angle, gray_in=None, num_expected=None, param=35):
    # MDT create a arg for this function for the expected
    # number of lines with that angle. Iteratively tune the params
    # until the houghlines below finds that many lines for that angle.
    line_img_in = np.copy(img_in)
    if gray_in is not None:
        gray = gray_in
    else:
        gray = cv2.cvtColor(img_in, cv2.COLOR_BGR2GRAY)
    # Look at using L2 gradient flag for canny below
    edges = cv2.Canny(gray, 20, 200, apertureSize = 7)
    #cv2.imshow('canny', edges)
    #cv2.waitKey()

    #lines = cv2.HoughLines(edges, 1, np.pi/180, 2)
    lines = cv2.HoughLines(edges, 1, np.pi/180, param)
    # 80 for traffic lights
    # 35 for stop signs
    # houghLines = cv2.HoughLinesP(canny, 1, np.pi/180, 100,
            # minLineLength = 250, maxLineGap = 100)
    if lines is None:
        return None

    #print "{} Lines found".format(len(lines))

    results_count = 0

    filtered_lines = []
    for rho, theta in lines[0]:
        skip = False
        for r,t in filtered_lines:
            if np.abs(np.abs(r) - np.abs(rho)) < 6:
                skip = True
        if skip or theta > angle + 0.008 or theta < angle - 0.008:
            continue

        results_count += 1
        a = np.cos(theta)
        b = np.sin(theta)
        x0 = a*rho
        y0 = b*rho
        x1 = int(x0 + 1000*(-b))
        y1 = int(y0 + 1000*(a))
        x2 = int(x0 - 1000*(-b))
        y2 = int(y0 - 1000*(a))
        # x1 = int(x0 + (-b))
        # y1 = int(y0 + (a))
        # x2 = int(x0 - (-b))
        # y2 = int(y0 - (a))

        filtered_lines.append((rho, theta))

        cv2.line(line_img_in,(x1,y1),(x2,y2),(0,255,255),2)

        if num_expected is not None and results_count == num_expected:
            break

    #cv2.imshow('hough_lines', line_img_in)
    #cv2.waitKey()

    if len(filtered_lines) != num_expected:
        #print "Error: expected {} lines not found".format(num_expected)
        pass

    return filtered_lines


def hough_lines_with_angle_stop_sign(img_in, angle, gray_in=None, num_expected=None, param=35):
    # MDT create a arg for this function for the expected
    # number of lines with that angle. Iteratively tune the params
    # until the houghlines below finds that many lines for that angle.
    line_img_in = np.copy(img_in)
    if gray_in is not None:
        gray = gray_in
    else:
        gray = cv2.cvtColor(img_in, cv2.COLOR_BGR2GRAY)
    # Look at using L2 gradient flag for canny below
    edges = cv2.Canny(gray, 50, 150, apertureSize = 3)
    #cv2.imshow('canny', edges)
    #cv2.waitKey()

    #lines = cv2.HoughLines(edges, 1, np.pi/180, 2)
    lines = cv2.HoughLines(edges, 1, np.pi/180, param)
    # 80 for traffic lights
    # 35 for stop signs
    # houghLines = cv2.HoughLinesP(canny, 1, np.pi/180, 100,
            # minLineLength = 250, maxLineGap = 100)
    if lines is None:
        return None

    #print "{} Lines found".format(len(lines))

    results_count = 0

    filtered_lines = []
    for rho, theta in lines[0]:
        skip = False
        for r,t in filtered_lines:
            if np.abs(np.abs(r) - np.abs(rho)) < 6:
                skip = True
        if skip or theta > angle + 0.008 or theta < angle - 0.008:
            continue

        results_count += 1
        a = np.cos(theta)
        b = np.sin(theta)
        x0 = a*rho
        y0 = b*rho
        x1 = int(x0 + 1000*(-b))
        y1 = int(y0 + 1000*(a))
        x2 = int(x0 - 1000*(-b))
        y2 = int(y0 - 1000*(a))
        # x1 = int(x0 + (-b))
        # y1 = int(y0 + (a))
        # x2 = int(x0 - (-b))
        # y2 = int(y0 - (a))

        filtered_lines.append((rho, theta))

        cv2.line(line_img_in,(x1,y1),(x2,y2),(0,255,255),2)

        if num_expected is not None and results_count == num_expected:
            break

    #cv2.imshow('hough_lines', line_img_in)
    #cv2.waitKey()

    if len(filtered_lines) != num_expected:
        #print "Error: expected {} lines not found".format(num_expected)
        pass

    return filtered_lines







def traffic_light_detection(img_in, radii_range):
    """Finds the coordinates of a traffic light image given a radii
    range.

    Use the radii range to find the circles in the traffic light and
    identify which of them represents the yellow light.

    Analyze the states of all three lights and determine whether the
    traffic light is red, yellow, or green. This will be referred to
    as the 'state'.

    It is recommended you use Hough tools to find these circles in
    the image.

    The input image may be just the traffic light with a white
    background or a larger image of a scene containing a traffic
    light.

    Args:
        img_in (numpy.array): image containing a traffic light.
        radii_range (list): range of radii values to search for.

    Returns:
        tuple: 2-element tuple containing:
        coordinates (tuple): traffic light center using the (x, y)
                             convention.
        state (str): traffic light state. A value in {'red', 'yellow',
                     'green'}
    """
    hsv_img_in = cv2.cvtColor(img_in, cv2.COLOR_BGR2HSV)

    black_stop_lower = np.array([0,0,0])
    black_stop_upper = np.array([255,255,100])
    blackish_mask = cv2.inRange(hsv_img_in, black_stop_lower, black_stop_upper)

    #cv2.imshow('blackish_mask', blackish_mask)
    #cv2.waitKey()

    # Alternative: search by unlit green or unlit red and search area
    #   around that bounded by shape of traffic light
    #import ipdb; ipdb.set_trace()
    boundaries = hough_lines_with_angle(img_in, 0.0, blackish_mask, 2, param=50)
    if boundaries is None or len(boundaries) < 2:
        left_col = 0
        right_col = img_in.shape[1]
        width = right_col - left_col #do i need this?
    else:
        sort_boundaries = sorted(boundaries, key=lambda x: x[0])
        left_col = int(sort_boundaries[0][0])
        right_col = int(sort_boundaries[1][0])
    boundaries = hough_lines_with_angle(img_in, 1.5707, blackish_mask, 2, param=50)
    # import ipdb; ipdb.set_trace()
    if boundaries is None or len(boundaries) < 2:
        top_row = 0
        bottom_row = img_in.shape[0]
        height = bottom_row - top_row #do i need this?
    else:
        sort_boundaries = sorted(boundaries, key=lambda x: x[0])
        top_row = int(sort_boundaries[0][0])
        bottom_row = int(sort_boundaries[1][0])
        height = bottom_row - top_row #do i need this?

    #import ipdb; ipdb.set_trace()
    bounded_img_in = img_in[top_row:bottom_row, left_col:right_col]

    #cv2.imshow('bounded_img_in', bounded_img_in)
    #cv2.waitKey()

    # import ipdb; ipdb.set_trace()
    hsv_img_in = cv2.cvtColor(bounded_img_in, cv2.COLOR_BGR2HSV)

    # Works for all lit or not
    #red_lower = np.array([0,80,80])
    #red_upper = np.array([20,255,255])
    #yellow_lower = np.array([25,105,105])
    #yellow_upper = np.array([35,255,255])
    #green_lower = np.array([50,105,105])
    #green_upper = np.array([70,255,255])

    # Brite lit check
    red_lower = np.array([0,180,180])
    red_upper = np.array([20,255,255])
    yellow_lower = np.array([25,195,195])
    yellow_upper = np.array([35,255,255])
    green_lower = np.array([50,195,195])
    green_upper = np.array([70,255,255])

    red_mask = cv2.inRange(hsv_img_in, red_lower, red_upper)
    #cv2.imshow('red_mask', red_mask)
    #cv2.waitKey()
    yellow_mask = cv2.inRange(hsv_img_in, yellow_lower, yellow_upper)
    #cv2.imshow('yellow_mask', yellow_mask)
    #cv2.waitKey()
    green_mask = cv2.inRange(hsv_img_in, green_lower, green_upper)
    #cv2.imshow('green_mask', green_mask)
    #cv2.waitKey()

    colors = ['red', 'yellow', 'green']
    masks = [red_mask, yellow_mask, green_mask]
    circles_per_color = {}
    for color, mask in zip(colors, masks):
        # cv2.imshow('{}_mask'.format(color), mask)
        # cv2.waitKey()
        for radius in reversed(radii_range):
            # Twiddle down param2 in a for-loop
            # for p2 in reversed(range(10,25)):
            # 28 is the perfect radius size for the first example
            circles = cv2.HoughCircles(mask,
                                       cv2.cv.CV_HOUGH_GRADIENT,
                                       1,
                                       radius,
                                       param1=50,
                                       param2=15 # play with this one the most
                                       ,
                                       # )
                                       minRadius=radius)
                                       # maxRadius=radius+50)

            if circles is not None:
                # print "Found {} circles at: {}. Color: {}".format(
                        # len(circles[0]), radius, color)
                if len(circles[0]) > 1:
                    print "WARNING: more than one bright lit circle found"
                circles = np.uint16(np.around(circles))
                circles_per_color[color] = circles
                # Continue on to the next color
                break

    if not circles_per_color:
        print "WARNING: No found bright lit circles"
        return None

    # for color, circles in circles_per_color.iteritems():
        # for i in circles[0,:]:
            # draw the outer circle
            # cv2.circle(bounded_img_in,(i[0],i[1]),i[2],(0,255,0),2)
            # draw the center of the circle
            # cv2.circle(bounded_img_in,(i[0],i[1]),2,(0,0,255),3)

    # cv2.imshow('input_color_all_circles', bounded_img_in)
    # cv2.waitKey()

    # Find the brightly lit circle
    bright_lit = None
    if len(circles_per_color) > 1:
        print "ERROR: Found more than one bright lit circle"
        return None
    else:
        # import ipdb; ipdb.set_trace()
        bright_lit = circles_per_color.keys()[0]
    #for color, circles in circles_per_color.iteritems():
        # import ipdb; ipdb.set_trace()
        # return (circles[0,0,0], circles[0,0,1]), color

    # print "Bright Lit is color: {}".format(bright_lit)

    if bright_lit == 'yellow':
        bright_yellow_circle = circles_per_color['yellow']
        return ((bright_yellow_circle[0,0,0]+left_col, bright_yellow_circle[0,0,1]+top_row)
                , 'yellow')

    # If the brightly lit circle was not yellow, then find the yellow circle
    bounded_img_hsv = cv2.cvtColor(bounded_img_in, cv2.COLOR_BGR2HSV)
    # define range of yellow color in HSV
    # base of yellow color in HSV [30 255 255]
    yellow_lower = np.array([20,100,100])
    yellow_upper = np.array([40,255,255])
    yellowish_mask = cv2.inRange(bounded_img_hsv, yellow_lower, yellow_upper)

    # cv2.imshow('yellowish', yellowish_mask)
    # cv2.waitKey()
    # import ipdb; ipdb.set_trace()
    for radius in reversed(radii_range):
        # 28 is the perfect radius size for the first example
        circles = cv2.HoughCircles(yellowish_mask,
                                   cv2.cv.CV_HOUGH_GRADIENT,
                                   1,
                                   radius,
                                   param1=50,
                                   param2=15 # play with this one the most
                                   ,
                                   # )
                                   minRadius=radius)
                                   # maxRadius=radius+50)

        if circles is not None:
            # print "Yellowish: Found {} circles at: {}.".format(
                    # len(circles[0]), radius)
            if len(circles[0]) > 1:
                print "WARNING: more than one yellowish circle found"
            circles = np.uint16(np.around(circles))
            # Continue on to the next color
            break

    #for i in circles[0,:]:
        # draw the outer circle
        #cv2.circle(bounded_img_in,(i[0],i[1]),i[2],(0,255,0),2)
        # draw the center of the circle
        #cv2.circle(bounded_img_in,(i[0],i[1]),2,(0,0,255),3)

    # cv2.imshow('yellowish_circles', bounded_img_in)
    # cv2.waitKey()

    # import ipdb; ipdb.set_trace()
    if len(circles[0]) < 1:
        print "ERROR: No found yellowish circles"
        return

    center = circles[0,0,0]+left_col, circles[0,0,1]+top_row
    return center, bright_lit


def yield_sign_detection(img_in):
    """Finds the centroid coordinates of a yield sign in the provided
    image.

    Args:
        img_in (numpy.array): image containing a traffic light.

    Returns:
        (x,y) tuple of coordinates of the center of the yield sign.
    """
    hsv_img_in = cv2.cvtColor(img_in, cv2.COLOR_BGR2HSV)

    #red = np.array([0,255,255])
    red_lower = np.array([0,235,235])
    red_upper = np.array([5,255,255])
    red_mask = cv2.inRange(hsv_img_in, red_lower, red_upper)

    boundaries = hough_lines_with_angle(img_in, 1.5707, red_mask, 1)
    if boundaries is None or len(boundaries) == 0:
        return None
    top_row = boundaries[0]

    boundaries = hough_lines_with_angle(img_in, 0.52359, red_mask, 1)
    if boundaries is None or len(boundaries) == 0:
        return None
    right_diagonal = boundaries[0]

    #import ipdb; ipdb.set_trace()
    boundaries = hough_lines_with_angle(img_in, 2.63544, red_mask, 1)
    #boundaries = hough_lines_with_angle(img_in, 2.61799, red_mask, 1)
    if boundaries is None or len(boundaries) == 0:
        return None
    left_diagonal = boundaries[0]

    # Find intersection of three lines
    a = np.array([[np.cos(top_row[1]), np.sin(top_row[1])],
                  [np.cos(left_diagonal[1]), np.sin(left_diagonal[1])]])
    b = [top_row[0], left_diagonal[0]]
    [x_int_tl, y_int_tl] = np.linalg.solve(a, b)

    a = np.array([[np.cos(top_row[1]), np.sin(top_row[1])],
                  [np.cos(right_diagonal[1]), np.sin(right_diagonal[1])]])
    b = [top_row[0], right_diagonal[0]]
    [x_int_tr, y_int_tr] = np.linalg.solve(a, b)

    a = np.array([[np.cos(left_diagonal[1]), np.sin(left_diagonal[1])],
                  [np.cos(right_diagonal[1]), np.sin(right_diagonal[1])]])
    b = [left_diagonal[0], right_diagonal[0]]
    [x_int_bc, y_int_bc] = np.linalg.solve(a, b)

    # cv2.circle(img_in, (x_int_tl, y_int_tl), 3, (255,0,0), -1)
    # cv2.circle(img_in, (x_int_tr, y_int_tr), 3, (255,0,0), -1)
    # cv2.circle(img_in, (x_int_bc, y_int_bc), 3, (255,0,0), -1)
    # cv2.imshow('draw', img_in)
    # cv2.waitKey()

    # Centroid is the average of the three vertices
    center = (int(np.rint((x_int_tl + x_int_tr + x_int_bc) / 3))) \
             , (int(np.rint((y_int_tl + y_int_tr + y_int_bc) / 3))) \

    return center


def stop_sign_detection(img_in):
    """Finds the centroid coordinates of a stop sign in the provided
    image.

    Args:
        img_in (numpy.array): image containing a traffic light.

    Returns:
        (x,y) tuple of the coordinates of the center of the stop sign.
    """
    # TODO Are stop signs squares? Even length on both directions?
    # FIXME investigat the above when detecting multiple signs
    #import ipdb; ipdb.set_trace()
    hsv_img_in = cv2.cvtColor(img_in, cv2.COLOR_BGR2HSV)

    # red_lower = np.array([0,100,100])
    # red_upper = np.array([10,255,255])
    red_stop = np.array([0,255,204])
    redish_mask = cv2.inRange(hsv_img_in, red_stop, red_stop)

    boundaries = hough_lines_with_angle_stop_sign(img_in, 0.0, redish_mask, 2)
    sort_boundaries = sorted(boundaries, key=lambda x: x[0])
    left_col = int(sort_boundaries[0][0])
    right_col = int(sort_boundaries[1][0])
    width = right_col - left_col

    bounded_img_in = img_in[:, left_col:right_col]

    boundaries = hough_lines_with_angle_stop_sign(bounded_img_in, 1.5707, redish_mask, 2)
    sort_boundaries = sorted(boundaries, key=lambda x: x[0])
    top_row = sort_boundaries[0][0]
    bottom_row = sort_boundaries[1][0]
    height = bottom_row - top_row

    #import ipdb; ipdb.set_trace()
    center = (int(left_col) + int(np.rint(width / 2))) \
             , (int(top_row) + int(np.rint(height / 2)))

    return center


def stop_sign_detection_noisy(img_in):
    """Finds the centroid coordinates of a stop sign in the provided
    image.

    Args:
        img_in (numpy.array): image containing a traffic light.

    Returns:
        (x,y) tuple of the coordinates of the center of the stop sign.
    """
    # TODO Are stop signs squares? Even length on both directions?
    # FIXME investigat the above when detecting multiple signs
    #import ipdb; ipdb.set_trace()
    hsv_img_in = cv2.cvtColor(img_in, cv2.COLOR_BGR2HSV)

    red_lower = np.array([0,180,200])
    red_upper = np.array([5,255,214])
    #red_stop = np.array([0,255,204])
    redish_mask = cv2.inRange(hsv_img_in, red_lower, red_upper)

    white_lower = np.array([0,0,230])
    white_upper = np.array([255,10,255])
    #red_stop = np.array([0,255,204])
    whitish_mask = cv2.inRange(hsv_img_in, white_lower, white_upper)

    redish_mask = cv2.bitwise_or(redish_mask, whitish_mask)

    boundaries = hough_lines_with_angle(img_in, 0.0, redish_mask, 2)
    if boundaries is None or len(boundaries) == 0:
        return None
    sort_boundaries = sorted(boundaries, key=lambda x: x[0])
    left_col = int(sort_boundaries[0][0])
    right_col = int(sort_boundaries[1][0])
    width = right_col - left_col

    bounded_img_in = img_in[:, left_col:right_col]

    boundaries = hough_lines_with_angle(bounded_img_in, 1.5707, redish_mask, 15)
    sort_boundaries = sorted(boundaries, key=lambda x: x[0])
    top_row = sort_boundaries[0][0]
    bottom_row = sort_boundaries[1][0]
    height = bottom_row - top_row

    #import ipdb; ipdb.set_trace()
    center = (int(left_col) + int(np.rint(width / 2))) \
             , (int(top_row) + int(np.rint(height / 2)))

    return center





def warning_sign_detection(img_in):
    """Finds the centroid coordinates of a warning sign in the
    provided image.

    Args:
        img_in (numpy.array): image containing a traffic light.

    Returns:
        (x,y) tuple of the coordinates of the center of the sign.
    """
    hsv_img_in = cv2.cvtColor(img_in, cv2.COLOR_BGR2HSV)

    yellow_lower = np.array([25,225,225])
    yellow_upper = np.array([35,255,255])
    yellow_mask = cv2.inRange(hsv_img_in, yellow_lower, yellow_upper)
    #cv2.imshow('yellow_mask', yellow_mask)
    #cv2.waitKey()

    boundaries = hough_lines_with_angle(img_in, 0.785398, yellow_mask, 2)

    if boundaries is None or len(boundaries) == 0:
        return None

    # Expect to get two lines here
    # sort by distance rho to get left side and right side
    sort_boundaries = sorted(boundaries, key=lambda x: x[0])
    top_left = sort_boundaries[0]
    bottom_right = sort_boundaries[1]

    boundaries = hough_lines_with_angle(img_in, 2.35619, yellow_mask, 2)
    # import ipdb; ipdb.set_trace()

    sort_boundaries = sorted(boundaries, key=lambda x: x[0])
    top_right = sort_boundaries[0]
    bottom_left = sort_boundaries[1]

    # Find intersection of three lines
    # import ipdb; ipdb.set_trace()
    a = np.array([[np.cos(top_left[1]), np.sin(top_left[1])],
                  [np.cos(bottom_left[1]), np.sin(bottom_left[1])]])
    b = [top_left[0], bottom_left[0]]
    [x_int_l, y_int_l] = np.linalg.solve(a, b)

    a = np.array([[np.cos(top_right[1]), np.sin(top_right[1])],
                  [np.cos(bottom_right[1]), np.sin(bottom_right[1])]])
    b = [top_right[0], bottom_right[0]]
    [x_int_r, y_int_r] = np.linalg.solve(a, b)

    a = np.array([[np.cos(top_left[1]), np.sin(top_left[1])],
                  [np.cos(top_right[1]), np.sin(top_right[1])]])
    b = [top_left[0], top_right[0]]
    [x_int_t, y_int_t] = np.linalg.solve(a, b)

    a = np.array([[np.cos(bottom_left[1]), np.sin(bottom_left[1])],
                  [np.cos(bottom_right[1]), np.sin(bottom_right[1])]])
    b = [bottom_left[0], bottom_right[0]]
    [x_int_b, y_int_b] = np.linalg.solve(a, b)

    # cv2.circle(img_in, (x_int_tl, y_int_tl), 3, (255,0,0), -1)
    # cv2.circle(img_in, (x_int_tr, y_int_tr), 3, (255,0,0), -1)
    # cv2.circle(img_in, (x_int_bc, y_int_bc), 3, (255,0,0), -1)
    # cv2.imshow('draw', img_in)
    # cv2.waitKey()

    # Centroid is the average of the three vertices
    center = (int(np.rint((x_int_l + x_int_r + x_int_t + x_int_b) / 4))) \
             , (int(np.rint((y_int_l + y_int_r + y_int_t + y_int_b) / 4))) \

    # cv2.circle(img_in, center, 3, (255,0,0), -1)
    # cv2.imshow('draw', img_in)
    # cv2.waitKey()

    return center


def construction_sign_detection(img_in):
    """Finds the centroid coordinates of a construction sign in the
    provided image.

    Args:
        img_in (numpy.array): image containing a traffic light.

    Returns:
        (x,y) tuple of the coordinates of the center of the sign.
    """
    hsv_img_in = cv2.cvtColor(img_in, cv2.COLOR_BGR2HSV)

    # define range of orange color in HSV
    # base of orange color in HSV [0 255 255]
    orange_lower = np.array([10, 235, 235])
    orange_upper = np.array([20, 255, 255])
    # orange_upper = np.array([41,100,100])

    orange_mask = cv2.inRange(hsv_img_in, orange_lower, orange_upper)
    #cv2.imshow('orange_mask', orange_mask)
    #cv2.waitKey()

    #import ipdb; ipdb.set_trace()
    boundaries = hough_lines_with_angle(img_in, 0.785398, orange_mask, 2)

    if boundaries is None or len(boundaries) == 0:
        return None

    # Expect to get two lines here
    # sort by distance rho to get left side and right side
    sort_boundaries = sorted(boundaries, key=lambda x: x[0])
    top_left = sort_boundaries[0]
    bottom_right = sort_boundaries[1]

    #show_lines_for_hough(img_in, top_left)
    #show_lines_for_hough(img_in, bottom_right)

    #import ipdb; ipdb.set_trace()
    boundaries = hough_lines_with_angle(img_in, 2.35619, orange_mask, 2)

    sort_boundaries = sorted(boundaries, key=lambda x: x[0])
    top_right = sort_boundaries[0]
    bottom_left = sort_boundaries[1]

    #show_lines_for_hough(img_in, top_right)
    #show_lines_for_hough(img_in, bottom_left)

    # Find intersection of three lines
    # import ipdb; ipdb.set_trace()
    a = np.array([[np.cos(top_left[1]), np.sin(top_left[1])],
                  [np.cos(bottom_left[1]), np.sin(bottom_left[1])]])
    b = [top_left[0], bottom_left[0]]
    [x_int_l, y_int_l] = np.linalg.solve(a, b)

    a = np.array([[np.cos(bottom_left[1]), np.sin(bottom_left[1])],
                  [np.cos(bottom_right[1]), np.sin(bottom_right[1])]])
    b = [bottom_left[0], bottom_right[0]]
    [x_int_b, y_int_b] = np.linalg.solve(a, b)

    a = np.array([[np.cos(top_right[1]), np.sin(top_right[1])],
                  [np.cos(bottom_right[1]), np.sin(bottom_right[1])]])
    b = [top_right[0], bottom_right[0]]
    [x_int_r, y_int_r] = np.linalg.solve(a, b)

    a = np.array([[np.cos(top_left[1]), np.sin(top_left[1])],
                  [np.cos(top_right[1]), np.sin(top_right[1])]])
    b = [top_left[0], top_right[0]]
    [x_int_t, y_int_t] = np.linalg.solve(a, b)

    # Centroid is the average of the three vertices
    center = (int(np.rint((x_int_l + x_int_r + x_int_t + x_int_b) / 4))) \
             , (int(np.rint((y_int_l + y_int_r + y_int_t + y_int_b) / 4))) \

    # cv2.circle(img_in, center, 3, (255,0,0), -1)
    # cv2.imshow('draw', img_in)
    # cv2.waitKey()

    return center


def do_not_enter_sign_detection(img_in):
    """Find the centroid coordinates of a do not enter sign in the
    provided image.

    Args:
        img_in (numpy.array): image containing a traffic light.

    Returns:
        (x,y) typle of the coordinates of the center of the sign.
    """
    circles_img_in = np.copy(img_in)

    hsv_img_in = cv2.cvtColor(img_in, cv2.COLOR_BGR2HSV)

    red = np.array([0,255,255])
    red_mask = cv2.inRange(hsv_img_in, red, red)
    #red_lower = np.array([0,230,230])
    #red_upper = np.array([2,255,255])

    #red_lower = np.array([0,180,180])
    #red_upper = np.array([5,255,255])
    #red_mask = cv2.inRange(hsv_img_in, red_lower, red_upper)
    #cv2.imshow('dne_red_mask', red_mask)
    #cv2.waitKey()

    radii_range = range(10, 30, 1)

    for radius in reversed(radii_range):
        # Twiddle down param2 in a for-loop
        # for p2 in reversed(range(10,25)):
        # 28 is the perfect radius size for the first example

        circles = cv2.HoughCircles(red_mask,
                                   cv2.cv.CV_HOUGH_GRADIENT,
                                   1,
                                   radius,
                                   param1=50,
                                   param2=15 # play with this one the most
                                   ,
                                   # )
                                   minRadius=radius)
                                   # maxRadius=radius+50)

        if circles is not None:
            # print "Found {} circles at: {}. Color: {}".format(
                    # len(circles[0]), radius, color)
            if len(circles[0]) > 1:
                print "WARNING: more than one bright lit red circle found"
            circles = np.uint16(np.around(circles))
            break

    if circles is None or len(circles) == 0:
        print "ERROR: No found bright lit red circles"
        return
    if len(circles) > 1:
        print "ERROR: Found more than one bright lit red circle"

    for i in circles[0,:]:
        # draw the outer circle
        cv2.circle(circles_img_in,(i[0],i[1]),i[2],(0,255,125),2)
        # draw the center of the circle
        cv2.circle(circles_img_in,(i[0],i[1]),2,(0,125,255),3)

    # cv2.imshow('input_color_all_circles', circles_img_in)
    # cv2.waitKey()

    return circles[0,0,0], circles[0,0,1]-2


def do_not_enter_sign_detection_noisy(img_in):
    """Find the centroid coordinates of a do not enter sign in the
    provided image.

    Args:
        img_in (numpy.array): image containing a traffic light.

    Returns:
        (x,y) typle of the coordinates of the center of the sign.
    """
    circles_img_in = np.copy(img_in)

    hsv_img_in = cv2.cvtColor(img_in, cv2.COLOR_BGR2HSV)

    #red = np.array([0,255,255])
    #red_mask = cv2.inRange(hsv_img_in, red, red)
    #red_lower = np.array([0,230,230])
    #red_upper = np.array([2,255,255])

    red_lower = np.array([0,180,180])
    red_upper = np.array([5,255,255])
    red_mask = cv2.inRange(hsv_img_in, red_lower, red_upper)
    #cv2.imshow('dne_red_mask', red_mask)
    #cv2.waitKey()

    radii_range = range(10, 30, 1)

    for radius in reversed(radii_range):
        # Twiddle down param2 in a for-loop
        # for p2 in reversed(range(10,25)):
        # 28 is the perfect radius size for the first example

        circles = cv2.HoughCircles(red_mask,
                                   cv2.cv.CV_HOUGH_GRADIENT,
                                   1,
                                   radius,
                                   param1=50,
                                   param2=15 # play with this one the most
                                   ,
                                   # )
                                   minRadius=radius)
                                   # maxRadius=radius+50)

        if circles is not None:
            # print "Found {} circles at: {}. Color: {}".format(
                    # len(circles[0]), radius, color)
            if len(circles[0]) > 1:
                print "WARNING: more than one bright lit red circle found"
            circles = np.uint16(np.around(circles))
            break

    if circles is None or len(circles) == 0:
        print "ERROR: No found bright lit red circles"
        return
    if len(circles) > 1:
        print "ERROR: Found more than one bright lit red circle"

    for i in circles[0,:]:
        # draw the outer circle
        cv2.circle(circles_img_in,(i[0],i[1]),i[2],(0,255,125),2)
        # draw the center of the circle
        cv2.circle(circles_img_in,(i[0],i[1]),2,(0,125,255),3)

    # cv2.imshow('input_color_all_circles', circles_img_in)
    # cv2.waitKey()

    return circles[0,0,0], circles[0,0,1]-2


def traffic_sign_detection(img_in):
    """Finds all traffic signs in a synthetic image.

    The image may contain at least one of the following:
    - traffic_light
    - no_entry
    - stop
    - warning
    - yield
    - construction

    Use these names for your output.

    See the instructions document for a visual definition of each
    sign.

    Args:
        img_in (numpy.array): input image containing at least one
                              traffic sign.

    Returns:
        dict: dictionary containing only the signs present in the
              image along with their respective centroid coordinates
              as tuples.

              For example: {'stop': (1, 3), 'yield': (4, 11)}
              These are just example values and may not represent a
              valid scene.
    """
    center_dict = {}

    traffic = np.copy(img_in)
    radii_range = range(10, 30, 1)
    result = traffic_light_detection(traffic, radii_range)
    if result is not None:
        center_dict['traffic_light'] = result[0]

    dne = np.copy(img_in)
    center = do_not_enter_sign_detection(dne)
    if center is not None:
        center_dict['no_entry'] = center

    stop = np.copy(img_in)
    center = stop_sign_detection(stop)
    if center is not None:
        center_dict['stop'] = center

    warning = np.copy(img_in)
    center = warning_sign_detection(warning)
    if center is not None:
        center_dict['warning'] = center

    yld = np.copy(img_in)
    center = yield_sign_detection(yld)
    if center is not None:
        center_dict['yield'] = center

    construction = np.copy(img_in)
    center = construction_sign_detection(construction)
    if center is not None:
        center_dict['construction'] = center

    return center_dict


def traffic_sign_detection_noisy(img_in):
    """Finds all traffic signs in a synthetic noisy image.

    The image may contain at least one of the following:
    - traffic_light
    - no_entry
    - stop
    - warning
    - yield
    - construction

    Use these names for your output.

    See the instructions document for a visual definition of each
    sign.

    Args:
        img_in (numpy.array): input image containing at least one
                              traffic sign.

    Returns:
        dict: dictionary containing only the signs present in the
              image along with their respective centroid coordinates
              as tuples.

              For example: {'stop': (1, 3), 'yield': (4, 11)}
              These are just example values and may not represent a
              valid scene.
    """
    #cv2.imshow('input', img_in)
    #cv2.waitKey()

    # Apply a median filter to the image
    #median = cv2.medianBlur(img_in, 11)
    #cv2.imshow('median11', median)
    #cv2.waitKey()

    # Apply a median filter to the image
    #median = cv2.medianBlur(img_in, 13)
    #cv2.imshow('median13', median)
    #cv2.waitKey()
    # Apply a median filter to the image
    #median = cv2.medianBlur(img_in, 9)
    #cv2.imshow('median9', median)
    #cv2.waitKey()
    #gaussian = cv2.GaussianBlur(img_in, (5,5), 1)
    #cv2.imshow('gaussian', gaussian)
    #cv2.waitKey()

    center_dict = {}

    median = cv2.fastNlMeansDenoisingColored(img_in,None,10,10,7,21)
    #cv2.imshow('fastnl', median)
    #cv2.waitKey()
    median = cv2.fastNlMeansDenoisingColored(median,None,10,10,7,21)
    #cv2.imshow('fastnl2', median)
    #cv2.waitKey()
    median = cv2.fastNlMeansDenoisingColored(median,None,10,10,7,21)
    #cv2.imshow('fastnl3', median)
    #cv2.waitKey()
    median = cv2.fastNlMeansDenoisingColored(median,None,10,10,7,21)
    #cv2.imshow('fastnl4', median)
    #cv2.waitKey()
    median = cv2.fastNlMeansDenoisingColored(median,None,10,10,7,21)
    #cv2.imshow('fastnl5', median)
    #cv2.waitKey()
    median = cv2.bilateralFilter(median,9,70,70)
    #cv2.imshow('postbilateral', median)
    #cv2.waitKey()
    median = cv2.bilateralFilter(median,9,70,70)
    #cv2.imshow('postbilateral2', median)
    #cv2.waitKey()
    median = cv2.bilateralFilter(median,9,70,70)
    #cv2.imshow('postbilateral3', median)
    #cv2.waitKey()
    median = cv2.bilateralFilter(median,9,70,70)
    #cv2.imshow('postbilateral4', median)
    #cv2.waitKey()
    median = cv2.bilateralFilter(median,9,70,70)
    #cv2.imshow('postbilateral5', median)
    #cv2.waitKey()
    result = traffic_light_detection(median, range(10, 30, 1))
    if result is not None:
        center_dict['traffic_light'] = result[0]

    # Apply a median filter to the image
    center = do_not_enter_sign_detection_noisy(median)
    if center is not None:
        center_dict['no_entry'] = center

    center = stop_sign_detection_noisy(median)
    if center is not None:
        center_dict['stop'] = center

    center = warning_sign_detection(median)
    if center is not None:
        center_dict['warning'] = center

    center = yield_sign_detection(median)
    if center is not None:
        center_dict['yield'] = center

    center = construction_sign_detection(median)
    if center is not None:
        center_dict['construction'] = center

    return center_dict


def traffic_sign_detection_challenge(img_in):
    """Finds traffic signs in an real image

    See point 5 in the instructions for details.

    Args:
        img_in (numpy.array): input image containing at least one
                              traffic sign.

    Returns:
        dict: dictionary containing only the signs present in the
              image along with their respective centroid coordinates
              as tuples.

              For example: {'stop': (1, 3), 'yield': (4, 11)}
              These are just example values and may not represent a
              valid scene.
    """
    raise NotImplementedError
